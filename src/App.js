import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import './App.css';
import Header from './Header'
import Footer from './Footer'
import TodoItem from './TodoItem'
import { actions as todoActions } from './state/todo'

function App() {
  const dispatch = useDispatch()

  const [loading, setLoading] = useState(false)
  const [todo, setTodo] = useState('')
  
  const handleTodoChange = (e) => {
    setTodo(e.target.value)
  }

  const handleTodoSubmit = () => {
    setLoading(true)

    dispatch(todoActions.addTodo(todo))
      .then(() => {
        setLoading(false)
      })
    setTodo('')
  }

  const todos = useSelector(state => {
    return state.todo.todos
  })

  const state = useSelector(state => {
    return state
  })
  
  console.log(state)
  
  return (
    <div>
      <section className="todoapp">
        {loading && (
          <h1>Loading...</h1>
        )}
        <Header
          todo={todo}
          onTodoChange={handleTodoChange}
          onTodoSubmit={handleTodoSubmit}
        />

        <section className="main">
          <input id="toggle-all" className="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>

          <ul className="todo-list">
            {todos.map((todo, index) => (
              <TodoItem key={index} todo={todo} />
            ))}

            {/* <li className="completed">
              <div className="view">
                <input className="toggle" type="checkbox" checked onChange={() => {}} />
                <label>Taste JavaScript</label>
                <button className="destroy"></button>
              </div>
              <input className="edit" value="Create a TodoMVC template" onChange={() => {}} />
            </li>
            <li>
              <div className="view">
                <input className="toggle" type="checkbox" onChange={() => {}} />
                <label>Buy a unicorn</label>
                <button className="destroy"></button>
              </div>
              <input className="edit" value="Rule the web" onChange={() => {}} />
            </li> */}
          </ul>
        </section>

        <Footer />
      </section>
    </div>
  );
}

export default App;
