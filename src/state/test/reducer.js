const init = {
    random: Math.random()
}

function reducer(state = init, action) {
    switch (action.type) {
        case 'test':
            return {
                ...state,
                random: action.payload
            }
        default:
            return state
    }
}

export default reducer
