import { ADD_TODO } from './constants'

const init = {
    loading: false,
    todos: []
}

function reducer(state = init, action) {
    switch (action.type) {
        case ADD_TODO:
            // Immutate
            const newTodo = action.payload
            return {
                ...state,
                loading: false,
                todos: [...state.todos, newTodo]
            }
        default:
            return state
    }
}

export default reducer
