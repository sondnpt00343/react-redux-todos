import { ADD_TODO } from './constants'

// Fake API
function createTodo(todo) {
    return new Promise(function (resolve) {
        setTimeout(() => {
            resolve({
                data: 'Đã tạo: ' + todo
            })
        }, 5000)
    })
}

export function addTodo(todo) {
    return async (dispatch) => {
        const res = await createTodo(todo)
        dispatch({
            type: ADD_TODO,
            payload: res.data
        })
    }
}

