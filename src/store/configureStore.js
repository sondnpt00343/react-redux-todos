import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import rootReducer from './reducers'

function configureStore() {
    const enhancers = [thunk]
    if (process.env.NODE_ENV !== 'production') {
        enhancers.push(logger)
    }
    const store = createStore(rootReducer, applyMiddleware(...enhancers))

    return store
}

export default configureStore
