import { combineReducers } from 'redux'
import { reducer as todoReducer } from '../state/todo'
import { reducer as testReducer } from '../state/test'

const rootReducer = combineReducers({
    todo: todoReducer,
    test: testReducer,
})

export default rootReducer
