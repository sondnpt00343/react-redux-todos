function Header({
    todo,
    onTodoChange,
    onTodoSubmit,
}) {
    return (
        <header className="header">
            <h1>todos</h1>
            <input
                className="new-todo"
                placeholder="What needs to be done?"
                autoFocus
                value={todo}
                onChange={onTodoChange}
                onKeyUp={e => e.keyCode === 13 && onTodoSubmit()}
            />
        </header>
    )
}

export default Header
