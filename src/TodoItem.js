function TodoItem({
    todo
}) {
    // className="completed"
    return (
        <li>
            <div className="view">
                <input className="toggle" type="checkbox" checked onChange={() => { }} />
                <label>{todo}</label>
                <button className="destroy"></button>
            </div>
            <input className="edit" value="Create a TodoMVC template" onChange={() => { }} />
        </li>
    )
}

export default TodoItem
